import React from 'react';
import './styles/Footer.css';

const Footer = () => {
    return (
        <footer className="footer">
            <p>© 2024 Todos los derechos reservados</p>
        </footer>
    );
};

export default Footer;
