import React from "react";
import logo from "./images/logo nb.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInstagram } from "@fortawesome/free-brands-svg-icons";
import "./styles/Home.css";

const Home = () => {
  return (
    <div className="home-container">
      <img src={logo} alt="Logo" className="home-logo" />
      <div className="mensaje-temp">
        <p>SITIO EN CONSTRUCCIÓN</p>
      </div>
      <div className="soccer-ball"></div>
      <div className="social-icons mt-4">
        <a
          href="https://www.instagram.com/juniorbfc.ea/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FontAwesomeIcon icon={faInstagram} size="2x" />
        </a>
      </div>
    </div>
  );
};

export default Home;
